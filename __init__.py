from st3m.application import Application, ApplicationContext
from st3m.goose import List
from st3m.input import InputState
from st3m.input import InputController
from st3m.reactor import Responder

from ctx import Context
import st3m.run

#
# Page used to show where the House of Tea is located into camp
#
class WhereIsTeahouse(Responder):
    def __init__(self) -> None:
        self.active = False

    def draw(self, ctx: Context) -> None:
        ctx.image_smoothing = False
        # Black background
        ctx.rectangle(-120, -120, 240, 240)
        ctx.rgb(0, 0, 0)
        ctx.fill()
        # Insert map image
        ctx.image(
            "/flash/sys/apps/house_of_tea/map.png",
            -120,
            -120,
            240,
            240,
        )

    def think(self, ins: InputState, delta_ms: int) -> None:
        if ins.buttons.app == ins.buttons.PRESSED_DOWN:
            self.active = False

    def is_active(self) -> bool:
        return self.active

    def show(self) -> None:
        self.active = True

#
# Overlay menu used to access info/pages
#
class Menu(Responder):
    def __init__(self) -> None:
        self.selected = -1   # Currently selected menu entry
        self.button_flag = 0 # Intermediate states for buttons
        self.delay    =  0   # Used to compute menu timeout
        self.menus: List[str] = [
            "Where is TeaHouse ?",
        ]

    def draw(self, ctx: Context) -> None:
        # Insert shadow over background
        ctx.rgba(0, 0, 0, 0.8).rectangle(-120, -120, 240, 240).fill()
        # Insert menu entries
        ctx.start_group()
        ctx.text_align = ctx.MIDDLE
        ctx.font = "Camp Font 2"
        ctx.font_size = 30
        # Title
        ctx.rgb(255,255, 255)
        ctx.move_to(0, -50)
        ctx.text("Menu")
        # Insert menu entries
        for i in range(1):
            if self.selected == i:
                ctx.rgb(255,255, 0)
            else:
                ctx.rgb(255,255,255)
            ctx.move_to(0, -25 + (i * 25))
            ctx.text(self.menus[i])

    def think(self, ins: InputState, delta_ms: int) -> int:
        if self.selected == -1:
            return

        # Update delay counter
        self.delay += delta_ms;
        # After 5 sec of inactivity, close menu
        if self.delay > 5000:
            self.close()

        if self.button_flag == 0:
            # Open menu or select previous menu line
            if ins.buttons.app == ins.buttons.PRESSED_LEFT:
                self.button_flag = 1
                self.select(2)
            # Open menu or select next menu line
            elif ins.buttons.app == ins.buttons.PRESSED_RIGHT:
                self.button_flag = 1
                self.select(1)
            # Activate current menu entry
            elif ins.buttons.app == ins.buttons.PRESSED_DOWN:
                self.button_flag = 2
        # A button have been pressed, wait for release
        else:
            if self.button_flag == 1 and ins.buttons.app == ins.buttons.NOT_PRESSED:
                self.button_flag = 0
            elif self.button_flag == 2 and ins.buttons.app == ins.buttons.NOT_PRESSED:
                self.button_flag = 0
                selected = self.get_current()
                self.close()
                return selected
        return -1

    def close(self) -> None:
        self.selected = -1
        self.delay = 0

    def get_current(self) -> int:
        return self.selected

    def is_active(self) -> bool:
        if self.selected == -1:
            return False
        return True

    def select(self, dir: int) -> None:
        if dir == 0:
            self.close()
        elif self.selected == -1:
            self.selected = 0 # Select first menu entry
        elif dir == 1 and self.selected < 0:
            self.selected += 1
        elif dir == 2 and self.selected > 0:
            self.selected -= 1
        # Reset menu timeout
        self.delay = 0

class HouseOfTea(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.menu = Menu()
        self.page_where = WhereIsTeahouse()

    def think(self, ins: InputState, delta_ms: int) -> None:
        if self.page_where.is_active():
            self.page_where.think(ins, delta_ms)
            return
        elif self.menu.is_active():
            sel = self.menu.think(ins, delta_ms)
            if sel == 0:
                self.page_where.show()
        else:
            # Left/Right buttons are used to open menu
            if ins.buttons.app == ins.buttons.PRESSED_LEFT:
                self.menu.select(1)
            elif ins.buttons.app == ins.buttons.PRESSED_RIGHT:
                self.menu.select(1)


    def draw(self, ctx: Context) -> None:
        if self.page_where.is_active():
            self.page_where.draw(ctx)
        else:
            ctx.image_smoothing = False
            ctx.rectangle(-120, -120, 240, 240)
            ctx.rgb(0, 0.60, 0.1)
            ctx.fill()

            ctx.image(
                "/flash/sys/apps/house_of_tea/teapot.png",
                -120,
                -120,
                240,
                240,
            )
            if self.menu.is_active():
                self.menu.draw(ctx)

# Continue to make runnable via mpremote run.
if __name__ == '__main__':
    st3m.run.run_view(HouseOfTea(ApplicationContext()))
